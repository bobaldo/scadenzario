package davidepatrizi.com.scadenzario.utility;

/**
 * Created by Bobaldo on 03/07/2015.
 */
public class Constant {
    public static final String NOTIFICATION_TITOLO = "titolo_notifica";
    public static final String NOTIFICATION_TARGA = "targa_notifica";
    public static final String NOTIFICATION_CONTENUTO = "testo_notifica";
}