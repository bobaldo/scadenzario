package davidepatrizi.com.scadenzario;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.widget.TextView;

import davidepatrizi.com.scadenzario.utility.Constant;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Bundle extras = getIntent().getExtras();
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                if (extras != null) {
                    setTitle(extras.getString(Constant.NOTIFICATION_TITOLO));
                    ((TextView) findViewById(R.id.txtTarga)).setText(extras.getString(Constant.NOTIFICATION_TARGA));
                    ((TextView) findViewById(R.id.txtContenuto)).setText(extras.getString(Constant.NOTIFICATION_CONTENUTO));
                }
            }
        });
    }
}