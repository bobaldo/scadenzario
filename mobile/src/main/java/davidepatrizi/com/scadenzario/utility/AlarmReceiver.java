package davidepatrizi.com.scadenzario.utility;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import davidepatrizi.com.scadenzario.NotificationActivity;
import davidepatrizi.com.scadenzario.R;

/**
 * Created by Bobaldo on 28/03/2015.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private AlarmManager am;
    private Intent intentAllarme;
    private PendingIntent pendingIntentAllarme;

    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("PD: ", "receive");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Resources res = context.getResources();
            int tipoAlarm = extras.getInt(Constant.TIPO_ALARM);
            String scadenza = DateManage.setDate(extras.getString(Constant.SCADENZA), Constant.formatterYYYYMMDD).toString();
            String targa = extras.getString(Constant.TARGA);
            String titolo = "";
            String contenuto = "";
            Log.w("PD: ", "tipoAlarm: " + tipoAlarm);
            Log.w("PD: ", "vedi_scadenza: " + scadenza);
            Log.w("PD: ", "targa: " + targa);

            switch (tipoAlarm) {
                case Constant.ALARM_SCADENZA_ASSICURAZIONE:
                    titolo = res.getString(R.string.scadenza_assicurazione);
                    contenuto = String.format(res.getString(R.string.notifica_scadenza_assicurazione), targa.toUpperCase(), scadenza);
                    break;
                case Constant.ALARM_SCADENZA_BOLLO:
                    titolo = res.getString(R.string.scadenza_bollo);
                    contenuto = String.format(res.getString(R.string.notifica_scadenza_bollo), targa.toUpperCase(), scadenza);
                    break;
            }

            Intent intentNotifica = new Intent(context, NotificationActivity.class);
            intentNotifica.putExtra(Constant.NOTIFICATION_TITOLO, titolo);
            intentNotifica.putExtra(Constant.NOTIFICATION_TARGA, targa);
            intentNotifica.putExtra(Constant.NOTIFICATION_CONTENUTO, contenuto);
            PendingIntent pendingIntentNotifica = PendingIntent.getActivity(
                    context,
                    0,
                    intentNotifica,
                    Intent.FLAG_ACTIVITY_NEW_TASK);

            Notification notification = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.icona)
                    .setContentTitle(titolo)
                    .setColor(Color.RED)
                    .extend(new NotificationCompat.WearableExtender()
                            .setContentIcon(R.drawable.icona))
                    .setContentIntent(pendingIntentNotifica)
                    .setContentText(contenuto)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .build();

            switch (tipoAlarm) {
                case Constant.ALARM_SCADENZA_ASSICURAZIONE:
                    nm.notify(Constant.NOTIFICA_SCADENZA_ASSICURAZIONE, notification);
                    break;
                case Constant.ALARM_SCADENZA_BOLLO:
                    nm.notify(Constant.NOTIFICA_SCADENZA_BOLLO, notification);
                    break;
            }
        }
        Log.w("PD: ", "notifica invitata");
    }

    public void setAlarm(Context context, int tipoAlarm, String _scadenza, String targa) throws ParseException {
        //ASSUNZIONE: l'alert viene inviato il mese prima della vedi_scadenza intorno alle 10:10
        Date date = Constant.formatterYYYYMMDD.parse(_scadenza);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 10);
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, month - 1);
        Log.w("PD", "tipoAlarm " + tipoAlarm);
        Log.w("PD", "vedi_scadenza: " + _scadenza + " allarme: " + DateManage.getDate(calendar));
        intentAllarme = new Intent(context, AlarmReceiver.class);
        intentAllarme.putExtra(Constant.TIPO_ALARM, tipoAlarm);
        intentAllarme.putExtra(Constant.SCADENZA, DateManage.getDate(calendar).toString());
        intentAllarme.putExtra(Constant.TARGA, targa);
        pendingIntentAllarme = PendingIntent.getBroadcast(context, 0, intentAllarme, 0);
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentAllarme);
        Log.w("PD", "Alarm setted");
    }

    public void cancelAlarm(Context context, int tipoAlarm, String scadenza, String targa) {
        intentAllarme = new Intent(context, AlarmReceiver.class);
        intentAllarme.putExtra(Constant.TIPO_ALARM, tipoAlarm);
        intentAllarme.putExtra(Constant.SCADENZA, scadenza);
        intentAllarme.putExtra(Constant.TARGA, targa);
        pendingIntentAllarme = PendingIntent.getBroadcast(context, 0, intentAllarme, 0);
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pendingIntentAllarme);
        Log.w("PD", "Alarm cancelled");
    }
}