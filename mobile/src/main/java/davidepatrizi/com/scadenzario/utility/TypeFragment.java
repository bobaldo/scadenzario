package davidepatrizi.com.scadenzario.utility;

/**
 * Created by Bobaldo on 11/05/2015.
 */
public enum TypeFragment {
    scelta,
    vedi_scadenza,
    vedi_info,
    vedi_tagliando
}