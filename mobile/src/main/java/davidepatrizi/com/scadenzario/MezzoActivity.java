package davidepatrizi.com.scadenzario;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import davidepatrizi.com.scadenzario.dba.ScadenzarioAdapterDB;
import davidepatrizi.com.scadenzario.dba.ScadenzarioDBEntry;
import davidepatrizi.com.scadenzario.fragment.ChooseFragment;
import davidepatrizi.com.scadenzario.fragment.InfoFragment;
import davidepatrizi.com.scadenzario.fragment.ScadenzeFragment;
import davidepatrizi.com.scadenzario.fragment.TagliandiFragment;
import davidepatrizi.com.scadenzario.utility.Constant;
import davidepatrizi.com.scadenzario.utility.TypeFragment;

public class MezzoActivity extends ActionBarActivity {
    private int _id_auto;
    private String _targa;
    private boolean _isStatoScelta = false;
    private TypeFragment _stato;

    @Override
    public void onResume() {
        super.onResume();
        show(_stato);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mezzo);
        this._stato = _stato.scelta;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            _id_auto = extras.getInt(ScadenzarioDBEntry.COLUMN_NAME_ID_AUTO);
            _targa = extras.getString(ScadenzarioDBEntry.COLUMN_NAME_TARGA);
            if (savedInstanceState == null) {
                show(_stato.scelta);
            }
        } else {
            Toast.makeText(this, "ID_AUTO NON PASSATO", Toast.LENGTH_LONG).show();
            this.goMainActivity();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case Constant.DIALOG_DELETE_CONFIRM:
                return new AlertDialog.Builder(this)
                        .setTitle(R.string.eliminazione_targa)
                        .setMessage(R.string.message_eliminazione_targa)
                        .setPositiveButton(R.string.confermo, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    ScadenzarioAdapterDB saDB = ScadenzarioAdapterDB.getInstance(getApplicationContext());
                                    saDB.deleteTarga(_id_auto);
                                    goMainActivity();
                                } catch (Exception ex) {
                                    Toast.makeText(getApplicationContext(), "Errore: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.annulla, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .create();
        }
        return null;
    }

    public void show(TypeFragment newStato) {
        Log.w("PD", "stato attuale: " + _stato + " nuovo stato: "+newStato);
        this._stato = newStato;
        if (this._stato.equals(_stato.vedi_info)) {
            _isStatoScelta = false;
            this.setTitle(getString(R.string.title_show_info) + " " + _targa.toUpperCase());
            showFragment(new InfoFragment(), true);
        } else if (this._stato.equals(_stato.vedi_tagliando)) {
            _isStatoScelta = false;
            this.setTitle(getString(R.string.title_show_tagliando) + " " + _targa.toUpperCase());
            showFragment(new TagliandiFragment(), true);
        } else if (this._stato.equals(_stato.vedi_scadenza)) {
            _isStatoScelta = false;
            this.setTitle(getString(R.string.title_show_scadenza) + " " + _targa.toUpperCase());
            showFragment(new ScadenzeFragment(), true);
        } else {
            _isStatoScelta = true;
            this.setTitle(getString(R.string.title_activity_mezzo) + " " + _targa.toUpperCase());
            showFragment(new ChooseFragment(), true);
        }
    }

    private void showFragment(Fragment f, boolean withIdAuto) {
        if (withIdAuto) {
            Bundle arguments = new Bundle();
            arguments.putInt(ScadenzarioDBEntry.COLUMN_NAME_ID_AUTO, _id_auto);
            arguments.putString(ScadenzarioDBEntry.COLUMN_NAME_TARGA, _targa);
            f.setArguments(arguments);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(android.R.id.content, f);
        transaction.addToBackStack(null); // permette di tornare alla lista con back
        transaction.commit();
    }

    public void goMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (_isStatoScelta) {
            //Toast.makeText(this, "BACK", Toast.LENGTH_LONG).show(); //debug line
            goMainActivity();
            return false;
        } else {
            _stato = TypeFragment.scelta;
            _isStatoScelta = true;
            return super.onKeyDown(keyCode, event);
        }
    }
}