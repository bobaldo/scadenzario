package davidepatrizi.com.scadenzario;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import davidepatrizi.com.scadenzario.adapter.ListTargaAdapter;
import davidepatrizi.com.scadenzario.dba.ScadenzarioAdapterDB;
import davidepatrizi.com.scadenzario.dba.ScadenzarioDBEntry;
import davidepatrizi.com.scadenzario.utility.Constant;

public class MainActivity extends ActionBarActivity {
    private ListView listView;
    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.txtListaTarghe);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Log.w("PD", "indice i: " + i);
                    openMezzoActivity((Cursor) listView.getItemAtPosition(i));
                } catch (Exception ex) {
                    Toast.makeText(context, "Errore: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnAggiungi = (Button) findViewById(R.id.btnAdd);
        btnAggiungi.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               showDialog(
                                                       Constant.DIALOG_NEW_MEZZO);
                                           }
                                       }
        );
        loadTarghe();
    }

    private void openMezzoActivity(Cursor cursor) {
        int _id = cursor.getInt(cursor.getColumnIndexOrThrow(ScadenzarioDBEntry._ID));
        String _targa = cursor.getString(cursor.getColumnIndexOrThrow(ScadenzarioDBEntry.COLUMN_NAME_TARGA));
        Intent intent = new Intent(context, MezzoActivity.class);
        intent.putExtra(ScadenzarioDBEntry.COLUMN_NAME_ID_AUTO, _id);
        intent.putExtra(ScadenzarioDBEntry.COLUMN_NAME_TARGA, _targa);
        startActivity(intent);
    }

    private void loadTarghe() {
        new AsyncTask<Void, Void, Cursor>() {
            @Override
            protected Cursor doInBackground(Void... voids) {
                ScadenzarioAdapterDB saDB = ScadenzarioAdapterDB.getInstance(context);
                Cursor c = saDB.getMezzo();
                if(c.getCount() == 1){
                    //TODO: passare automaticamente alla MezzoActivity
                    //openMezzoActivity(c);
                }
                return c;
            }

            @Override
            protected void onPostExecute(Cursor cursor) {
                try {
                    listView.setAdapter(new ListTargaAdapter(context, cursor));
                } catch (NullPointerException e) {
                }
            }
        }.execute();
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        ((TextView) ((AlertDialog) dialog).findViewById(R.id.txtTarga)).setText("");
        ((Spinner) ((AlertDialog) dialog).findViewById(R.id.txtTipo)).setSelection(0);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case Constant.DIALOG_NEW_MEZZO:
                LayoutInflater factory = LayoutInflater.from(this);
                return new AlertDialog.Builder(this)
                        .setTitle(R.string.btnNewMezzo)
                        .setView(factory.inflate(R.layout.dialog_new_mezzo, null))
                        .setPositiveButton(R.string.aggiungi, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String targa = ((TextView) ((AlertDialog) dialog).findViewById(R.id.txtTarga)).getText().toString();
                                String tipo = ((Spinner) ((AlertDialog) dialog).findViewById(R.id.txtTipo)).getSelectedItem().toString();
                                ScadenzarioAdapterDB saDB = ScadenzarioAdapterDB.getInstance(((AlertDialog) dialog).getContext());
                                saDB.insertTarga(targa, tipo);
                                loadTarghe();
                            }
                        })
                        .setNegativeButton(R.string.cancella, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .create();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_new:
                showDialog(Constant.DIALOG_NEW_MEZZO);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}