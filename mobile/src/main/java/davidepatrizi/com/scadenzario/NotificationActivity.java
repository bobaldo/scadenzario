package davidepatrizi.com.scadenzario;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import davidepatrizi.com.scadenzario.utility.Constant;


public class NotificationActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_notification);
        Log.w("PD: ", "NotificationActivity");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            setTitle(extras.getString(Constant.NOTIFICATION_TITOLO));
            ((TextView) findViewById(R.id.txtNotificaTarga)).setText(extras.getString(Constant.NOTIFICATION_TARGA));
            ((TextView) findViewById(R.id.txtNotificaContenuto)).setText(extras.getString(Constant.NOTIFICATION_CONTENUTO));
        }
    }
}